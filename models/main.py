# univariate lstm example
from numpy import array, arange, sin, pi
from pandas import read_csv
from keras.models import Sequential, model_from_json
from keras.layers import LSTM
from keras.layers import Dense
import matplotlib.pyplot as plt


def load_data():
    # define input sequence
    raw_seq = read_csv("data/combinedData.csv", index_col="Time")
    data = raw_seq.iloc[:, 0].values
    return data


# split a univariate sequence into samples
def split_sequence(sequence, n_steps):
    X, y = list(), list()
    for i in range(len(sequence)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the sequence
        if end_ix > len(sequence) - 1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = sequence[i:end_ix], sequence[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return array(X), array(y)


def reshape_data(toReshape, n_features):
    # reshape from [samples, timesteps] into [samples, timesteps, features]
    return toReshape.reshape((toReshape.shape[0], toReshape.shape[1], n_features))


def create_model(n_steps, n_features, X, y, test_portion):
    # define model
    model = Sequential()
    model.add(LSTM(50, activation="relu", input_shape=(n_steps, n_features)))
    model.add(Dense(1))
    model.compile(optimizer="adam", loss="mse", metrics=["mae", "mape"])
    # fit model
    model.fit(X, y, epochs=50, validation_split=test_portion)
    return model


def save_model(model):
    # serialize model to JSON
    model_json = model.to_json()
    with open("models/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("models/weights.h5")
    print("Saved model to disk")


def load_model(json, weights):
    # load json and create model
    json_file = open(json, "r")
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(weights)
    print("Loaded model from disk")
    return loaded_model


def evaluate_model(model, data, start_val, n_steps, features):
    # evaluate loaded model on test data
    model.compile(loss="mse", optimizer="adam", metrics=["mae", "mape"])
    test_set = data[start_val:]
    X, y = split_sequence(test_set, n_steps)
    X = reshape_data(X, features)
    yhat = model.predict(X)
    return yhat


def graph_model_accuracy(data, predictions, start_val):
    plt.plot(data[start_val:])
    plt.plot(predictions)
    plt.title("Prediction versus Actual for last %s data points" % (300000 - start_val))
    plt.show()


def main():
    data = load_data()

    # Features: number of input data 'columns'
    # Steps: number of data points to look at for each 'guessed' prediction during training
    n_steps = 3
    n_features = 1
    test_portion = 0.025

    # split into samples
    X, y = split_sequence(data, n_steps)
    X = reshape_data(X, n_features)

    try:
        model = load_model("models/model.json", "models/model.h5")
    except:
        model = create_model(n_steps, n_features, X, y, test_portion)

    start_val = int(len(data) * (1 - test_portion))
    predictions = evaluate_model(model, data, start_val, n_steps, n_features)

    graph_model_accuracy(data, predictions, start_val)


main()
