import numpy
import pandas as pd

oldData = pd.read_csv("./data/darkSky.csv").set_index("Time")

x = numpy.array([])
y = numpy.array([])
for i in range(0, oldData.shape[0]):
    x = numpy.append(arr=x, values=oldData.iloc[i].name)
    y = numpy.append(arr=y, values=oldData.iat[i, 0])

newData = oldData

for i in range(0, oldData.shape[0]):
    start = x[i]
    toAppend = pd.DataFrame(data={"Time": [], "Temperature": []}).set_index("Time")
    for j in range(1, 4):
        time = start + (900 * j)
        temperature = round(numpy.interp(time, x, y), 2)
        newVals = pd.DataFrame(
            data={"Time": [time], "Temperature": [temperature]}
        ).set_index("Time")
        toAppend = toAppend.append(other=newVals)
    newData = newData.append(other=toAppend)
    print(newData)

newData.sort_values(by="Time", inplace=True)
# newData.drop_duplicates(inplace=True)

newData.to_csv("CompleteWeatherData.csv")
