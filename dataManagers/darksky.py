import pandas as pd
import numpy
from requests import Session

config = {
    "lat": "37.367702",
    "long": "-79.152300",
    "key": "e1f03f6384556b05efd0a82587f85560",
    "startDate": "1262322000",
}


def getDarkSky(date):
    # This function gathers 24 1-hour data periods, converts the data to json, and returns the time and temperature data
    session = Session()

    data = session.get(
        "https://api.darksky.net/forecast/"
        + config["key"]
        + "/"
        + config["lat"]
        + ","
        + config["long"]
        + ","
        + date
        + "?"
        + "exclude=currently"
    ).json()

    try:
        return data["hourly"]["data"]
    except:
        print("Code: " + str(data["code"]))
        print("Error Message: " + data["error"])
        exit()


def cleanDarkSky(data):
    array = []
    # create an array of the darksky data. format: (time, temperature)
    for x in range(0, len(data)):
        array.append([data[x]["time"], data[x]["temperature"]])

    array = numpy.asarray(array)

    # create a pandas dataframe out of the array and remove one second from each item in the time column
    return pd.DataFrame(
        {"Time": (array[:, 0] - 1), "Temperature": array[:, 1]}
    ).set_index("Time")


for i in range(0, 1000):
    existingData = pd.read_csv("./data/darkSky.csv", index_col=0)
    seconds = existingData.index[-1]
    
    if seconds >= 1581397199.0:
      print("collected all DarkSky Data")
      exit()

    print("reading existing data in file starting at: " + str(seconds))

    dateForRun = str(int(seconds) + (86400))

    weatherData = cleanDarkSky(getDarkSky(dateForRun))

    existingData.append(weatherData).to_csv("./data/darkSky.csv")

    print(existingData)
